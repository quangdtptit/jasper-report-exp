package com.example.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.example.dto.ClazzDTO;
import com.example.dto.StudentDTO;
import com.example.service.StudentService;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class StudentServiceImpl implements StudentService {

	@Override
	public List<StudentDTO> getAll() {
		List<StudentDTO> students = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			StudentDTO student = new StudentDTO(UUID.randomUUID().toString(), "Name " + i, new Date(),
					i % 2 == 0 ? true : false, i + 0.2);
			students.add(student);
		}
		return students;
	}

	@Override
	public void reportPDFStudent() {

		List<ClazzDTO> clazzDTOs = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			clazzDTOs.add(new ClazzDTO(i + 1, "Nhóm " + (i + 1), i * 10));
		}

		List<StudentDTO> list = this.getAll();
		try {
			JasperReport jasperReport = JasperCompileManager
					.compileReport(new FileInputStream(new File("student.jrxml")));
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
			JRBeanCollectionDataSource dataSource2 = new JRBeanCollectionDataSource(clazzDTOs);
			Map<String, Object> map = new HashMap<>();
			map.put("StudentDataSource", dataSource);
			map.put("Student2DataSource", dataSource2);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, new JREmptyDataSource());
			OutputStream os = new FileOutputStream(new File("student.pdf"));
			JasperExportManager.exportReportToPdfStream(jasperPrint, os);
			System.out.println("DONE");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		StudentService service = new StudentServiceImpl();
		service.reportPDFStudent();
	}

}
