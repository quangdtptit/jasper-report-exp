package com.example.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import com.example.dto.OrderDTO;
import com.example.dto.OrderDetailDTO;
import com.example.dto.OrderReportPdfDTO;
import com.example.service.OrderService;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class OrderServiceImpl implements OrderService {

	/* FAKE DATA */
	private Random random = new Random();

	private List<OrderDetailDTO> getOrderDetailsRamdom() {
		int size = random.nextInt(9 - 3 + 1) + 3;

		List<OrderDetailDTO> result = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			OrderDetailDTO e = new OrderDetailDTO();
			e.setName("Product " + random.nextInt());
			e.setCode(UUID.randomUUID().toString());
			e.setDiscount(random.nextInt(10) * 10);
			e.setPrice(new BigDecimal((random.nextInt(10) + 1) * 1000000));
			e.setQuantity(random.nextInt(10 - 1 + 1) + 1);
			result.add(e);
		}
		return result;
	}

	private List<OrderDTO> getOrderDTOs() {
		List<OrderDTO> orderDTOs = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			OrderDTO orderDTO = new OrderDTO();
			orderDTO.setName("Hóa đơn số " + (i + 1));
			orderDTO.setDate(new Date());
			orderDTO.setOrderDetails(getOrderDetailsRamdom());
			int totalQuantity = 0;
			BigDecimal totalPrice = new BigDecimal(0);
			for (OrderDetailDTO e : orderDTO.getOrderDetails()) {
				totalQuantity += e.getQuantity();
				totalPrice.add(e.getPrice());
			}
			orderDTO.setTotalPrice(totalPrice);
			orderDTO.setTotalQuantity(totalQuantity);
			orderDTOs.add(orderDTO);
		}
		return orderDTOs;
	}
	/* END FAKE DATA */

	@Override
	public void reportPdf() {
		List<OrderDTO> orderDTOs = this.getOrderDTOs();
		List<OrderReportPdfDTO> result = new ArrayList<>();
		int no = 1;
		for (OrderDTO orderDTO : orderDTOs) {
			List<OrderDetailDTO> orderDetailDTOs = orderDTO.getOrderDetails();
			for (OrderDetailDTO orderDetailDTO : orderDetailDTOs) {
				OrderReportPdfDTO e = new OrderReportPdfDTO();
				e.setNo(no);
				e.setDateOrder(orderDTO.getDate());
				e.setNameOrder(orderDTO.getName());
				e.setCodeProduct(orderDetailDTO.getCode());
				e.setNameProduct(orderDetailDTO.getName());
				e.setPriceProduct(orderDetailDTO.getPrice().longValue());
				e.setQuantityProduct(orderDetailDTO.getQuantity());
				e.setAmount(orderDetailDTO.getQuantity() * orderDetailDTO.getPrice().longValue());

				result.add(e);
			}
			no++;
		}

		try {
			JasperReport jasperReport = JasperCompileManager
					.compileReport(new FileInputStream(new File("xmls/order_template.jrxml")));
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(result);
			Map<String, Object> map = new HashMap<>();
			map.put("OrderSource", dataSource);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, new JREmptyDataSource());
			OutputStream os = new FileOutputStream(new File("order_details.pdf"));
			JasperExportManager.exportReportToPdfStream(jasperPrint, os);
			System.out.println("===> PRINT DONE ...");
		} catch (FileNotFoundException | JRException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		OrderService orderService = new OrderServiceImpl();
		orderService.reportPdf3();

	}

	@Override
	public void reportPdf2() {
		List<OrderDTO> orderDTOs = this.getOrderDTOs();
		List<OrderReportPdfDTO> result = new ArrayList<>();
		int no = 1;
		for (OrderDTO orderDTO : orderDTOs) {
			List<OrderDetailDTO> orderDetailDTOs = orderDTO.getOrderDetails();
			for (OrderDetailDTO orderDetailDTO : orderDetailDTOs) {
				OrderReportPdfDTO e = new OrderReportPdfDTO();
				e.setNo(no);
				e.setDateOrder(orderDTO.getDate());
				e.setNameOrder(orderDTO.getName());
				e.setCodeProduct(orderDetailDTO.getCode());
				e.setNameProduct(orderDetailDTO.getName());
				e.setPriceProduct(orderDetailDTO.getPrice().longValue());
				e.setQuantityProduct(orderDetailDTO.getQuantity());
				e.setAmount(orderDetailDTO.getQuantity() * orderDetailDTO.getPrice().longValue());

				result.add(e);
			}
			no++;
		}

		try {
			JasperReport jasperReport = JasperCompileManager
					.compileReport(new FileInputStream(new File("xmls/order_report_tempalte_2.jrxml")));
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(result);
			Map<String, Object> map = new HashMap<>();
			map.put("OrderDataSource", dataSource);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, new JREmptyDataSource());
			OutputStream os = new FileOutputStream(new File("order_details_2.pdf"));
			JasperExportManager.exportReportToPdfStream(jasperPrint, os);
			System.out.println("===> PRINT DONE ...");
		} catch (FileNotFoundException | JRException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void reportPdf3() {
		List<OrderDTO> orderDTOs = this.getOrderDTOs();
		List<OrderReportPdfDTO> result = new ArrayList<>();
		int no = 1;
		for (OrderDTO orderDTO : orderDTOs) {
			List<OrderDetailDTO> orderDetailDTOs = orderDTO.getOrderDetails();
			for (OrderDetailDTO orderDetailDTO : orderDetailDTOs) {
				OrderReportPdfDTO e = new OrderReportPdfDTO();
				e.setNo(no);
				e.setDateOrder(orderDTO.getDate());
				e.setNameOrder(orderDTO.getName());
				e.setCodeProduct(orderDetailDTO.getCode());
				e.setNameProduct(orderDetailDTO.getName());
				e.setPriceProduct(orderDetailDTO.getPrice().longValue());
				e.setQuantityProduct(orderDetailDTO.getQuantity());
				e.setAmount(orderDetailDTO.getQuantity() * orderDetailDTO.getPrice().longValue());

				result.add(e);
			}
			no++;
		}
		int temp = -1;
		for(OrderReportPdfDTO dto : result) {
			int flag = dto.getNo();
			if(dto.getNo() == temp) {
				setBlankOrderReportDTO(dto);
			}
			temp = flag;
		}
		System.out.println(result);
		
		try {
			JasperReport jasperReport = JasperCompileManager
					.compileReport(new FileInputStream(new File("xmls/Blank_A4.jrxml")));
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(result);
			Map<String, Object> params = new HashMap<>();
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params,dataSource );
			OutputStream os = new FileOutputStream(new File("order_details_3.pdf"));
			JasperExportManager.exportReportToPdfStream(jasperPrint, os);
			System.out.println("===> PRINT DONE ...");
		} catch (FileNotFoundException | JRException e) {
			e.printStackTrace();
		}
	}
	
	private void setBlankOrderReportDTO(OrderReportPdfDTO dto) {
		dto.setDateOrder(null);
		dto.setNo(null);
		dto.setNameOrder("");
	}

//	private static int digit(int n) {
//		String str = n + "";
//		Stack<Integer> stack = new Stack<>();
//		stack.push(Integer.parseInt(str.charAt(0) + ""));
//		for (int i = 1; i < str.length(); i++) {
//			int a = Integer.parseInt(str.charAt(i) + "");
//			if(a)
//		}
//		return 0;
//	}

}
