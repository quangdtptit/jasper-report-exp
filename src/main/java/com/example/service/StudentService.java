package com.example.service;

import java.util.List;

import com.example.dto.StudentDTO;

public interface StudentService {
	
	List<StudentDTO> getAll();
	
	void reportPDFStudent();
}
