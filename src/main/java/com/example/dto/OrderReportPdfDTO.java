package com.example.dto;

import java.util.Date;

public class OrderReportPdfDTO {
	
	private Integer no;
	private Date dateOrder;
	private String nameOrder;
	private String codeProduct;
	private String nameProduct;
	private Long priceProduct;
	private int quantityProduct;
	private Long amount;

	

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public Date getDateOrder() {
		return dateOrder;
	}

	public void setDateOrder(Date dateOrder) {
		this.dateOrder = dateOrder;
	}

	public String getNameOrder() {
		return nameOrder;
	}

	public void setNameOrder(String nameOrder) {
		this.nameOrder = nameOrder;
	}

	public String getCodeProduct() {
		return codeProduct;
	}

	public void setCodeProduct(String codeProduct) {
		this.codeProduct = codeProduct;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	public Long getPriceProduct() {
		return priceProduct;
	}

	public void setPriceProduct(Long priceProduct) {
		this.priceProduct = priceProduct;
	}

	public int getQuantityProduct() {
		return quantityProduct;
	}

	public void setQuantityProduct(int quantityProduct) {
		this.quantityProduct = quantityProduct;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "OrderReportPdfDTO [no=" + no + ", dateOrder=" + dateOrder + ", nameOrder=" + nameOrder
				+ ", codeProduct=" + codeProduct + ", nameProduct=" + nameProduct + ", priceProduct=" + priceProduct
				+ ", quantityProduct=" + quantityProduct + ", amount=" + amount + "]\n";
	}
	

}
