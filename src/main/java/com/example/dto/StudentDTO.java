package com.example.dto;

import java.util.Date;

public class StudentDTO {
	
	private String code;
	private String name;
	private Date dob;
	private boolean gender;
	private double point;

	public StudentDTO() {
		super();
	}

	public StudentDTO(String code, String name, Date dob, boolean gender, double point) {
		super();
		this.code = code;
		this.name = name;
		this.dob = dob;
		this.gender = gender;
		this.point = point;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public double getPoint() {
		return point;
	}

	public void setPoint(double point) {
		this.point = point;
	}

}
