package com.example.dto;

public class ClazzDTO {
	private int no;
	private String name;
	private int totalStd;

	public ClazzDTO() {

	}

	public ClazzDTO(int no, String name, int totalStd) {
		super();
		this.no = no;
		this.name = name;
		this.totalStd = totalStd;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTotalStd() {
		return totalStd;
	}

	public void setTotalStd(int totalStd) {
		this.totalStd = totalStd;
	}

}
