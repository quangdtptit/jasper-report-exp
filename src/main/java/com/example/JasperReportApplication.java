package com.example;

import com.example.service.StudentService;
import com.example.service.impl.StudentServiceImpl;

public class JasperReportApplication {

	public static void main(String[] args) {
		StudentService service = new StudentServiceImpl();
		service.reportPDFStudent();
	}
}
